# React Skeleton
This is a skeleton project for a simple react app with webpack and babel. I know there is probably a more simple setup than this, however it is still fairly simple and introduces webpack and bundling. This skeleton does not use create-react-app to try to have as little boilerplate setup as possible. 

The main purpose for this project is for learning purposes for myself, whilst I am trying to learn modern front end technologies. However if anyone stumbles across this feel free to use it and if you spot any mistakes or have any suggestions then open an issue. 


# Setup
I have used yarn for this project, so first make sure you have yarn installed. 

    brew install yarn

Then clone the repo and in the root of the project run the following to install all of the dependencies:

    yarn install
To start up the project run the following:

    yarn start
This will start up a webpack dev server 

## What each dependency actually does or is used for
I find that when using tutorial its often the fact that they can skip over setup by just saying use a project generator (e.g. create-react-app) and they tell you to add a huge list of dependencies without say why or what the benefit is of using them. So I have tried to keep the dependencies to a minimum (although there are still more than I would like) and below I have attempted to give a breif bit of explanation of what each one does and why I have included it. Also some links for further reading.

### Dependencies 
#### react
The library that this project is based around. It is a javascript library for creating user interfaces. If you want to know more about react or start learning, visit there site: https://reactjs.org/
#### react-dom
This gives react access to the DOM and allows you to render your components to the DOM. You will pretty much always need this dependency if you are using react. Most of the time you will only use the `render()` method (or `hydrate` if you are server side rendering) from this library. However it also has some server side features such as the `renderToString` method which is used when server side rendering a component. Read more about it here: https://reactjs.org/docs/react-dom.html
### Dev dependencies
The following dependecies are dev dependencies because they are only need when transpiling and bundling your code. In production you will run from the bundled version of your code and there do not need and of the following dependencies to be installed. 
#### @babel/core
Babel is a compiler for javascript, which can transform your code so that it is backwards compatible with older javascript syntax. Babel supports the latest version of javascript. If you want to use ES6 feature such as import and export. You will need babel. @babel/core includes all the transformations needed to make your code backwards compatible, there are lots of other babel libraries with extra functionally that I recommend taking a look at: https://babeljs.io/docs/en/
#### @babel/preset-env
Babel is a preset that allows you to specify specific browsers version and runtimes that you want to support. It will then do the heavy lifting of finding what they support and transfom your code to whatever you want to target supports. You can read more about how to configure it here: https://babeljs.io/docs/en/babel-preset-env
#### @babel/preset-react
React uses its own syntax called jsx to express components. It isnt needed, however it makes react code a lot more readable, therefore most people use it. Jsx isnt part of vanilla javascript therefore it needs to be transformed into something that javascript can understand. @babel/preset-react is a preset that adds the transforms needed for this. Read more here: https://babeljs.io/docs/en/babel-preset-react
#### webpack
Webpack is a module bundler. It maps out a dependency graph for all of your static files in your project (e.g. js, css, images, etc.). It then creates one or more bundle files for your static content. You can then include for example your javascript bundle on a page and it will have all of your post processed javascript. There is a glossary for the terms used for webpack here: https://webpack.js.org/glossary/ and more info on webpack here (there is a whole bunch of other stuff webpack can do too): https://webpack.js.org/concepts/
#### webpack-cli
The command line interface for webpack, which allows you to run webpack commands and build your static content into bundles. Read more here: https://webpack.js.org/api/cli/
#### webpack-dev-server
Provides a development server with webpack, to allow you to quickly get a app up and running. It should only ever be used for development purposes. Provides some nice features such as live reloading. Read more here: https://github.com/webpack/webpack-dev-server
#### babel-loader
Babel-loader is a loader written for webpack. A loader basically allows files to be preprocessed. The babel-loader is a loader that loads javascript code and transforms it using babel. Read more here: https://github.com/babel/babel-loader
#### css-loader
A webpack loader for css files. It loads a css file and resolves any imports and urls and returns the resolved file. This doesnt actually insert the styles into a page however. Read more here: https://webpack.js.org/loaders/css-loader/
#### style-loader
A webpack loader for injecting css onto the DOM (makes the css available on the page). https://webpack.js.org/loaders/style-loader/
#### html-webpack-plugin
A plugin for webpack to make the creation of html files easier when using bundled static assets. For example if you are using a bundled javascript file, it will inject a script pointing to your bundled file into your html file. This means you dont have to worry about getting the paths correct. Read more here: https://github.com/jantimon/html-webpack-plugin
 
