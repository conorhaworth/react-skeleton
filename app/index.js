import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

const Hello = () => {
    return <h1>Hello, World!</h1>
}

ReactDOM.render(<Hello/>, document.getElementById('root'))
